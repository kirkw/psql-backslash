CREATE TABLE sessions (
    id INT8 generated always as identity PRIMARY KEY,
    user_id INT4 references users (id),
    created_on timestamptz NOT NULL DEFAULT now(),
    updated_on timestamptz,
    parent INT8 references sessions (id)
);
