#!/usr/bin/env bash

die() {
    echo "$@" >&2
    exit 1
}

show_help() {
    cat << _END_OF_HELP_
Syntax:
    $0 [-s SCHEMA] [-o OUTPUT]

Options:
    -s - name of schema that should contain all the functions, defaults to psql
    -o - where to output generated sql, defaults to full.create.sql in top project dir
_END_OF_HELP_
    exit 0
}

cd "$( dirname "$( dirname "$( readlink -f "$0" )" )" )" || die "Can't go to top dir of project?!"

use_schema=psql
output="full.create.sql"

while getopts 'o:s:h' optname
do
    case "${optname}" in
        s)  use_schema="${OPTARG}" ;;
        o)  output="${OPTARG}"     ;;
        *)  show_help              ;;
    esac
done

[[ "${use_schema}" =~ ^[a-z][a-z0-9_]+$ ]] || die "Invalid schema: ${use_schema}"

tmp_file="$( mktemp )"
trap 'rm -f "${tmp_file}"' EXIT

mapfile -t all_function_files < <( find functions -type f | sort -V )

(
    printf 'begin;\n'
    printf 'create schema if not exists @extschema@;\n'
    for file in "${all_function_files[@]}"
    do
        printf -- '-- %s\n' "${file}"
        cat "${file}"
        printf '\n'
    done
    printf 'commit;\n'
) > "${tmp_file}"

sed -e "s/@extschema@/${use_schema}/g" "${tmp_file}" > "${output}"

echo "All done."

exit 0
