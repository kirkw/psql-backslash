#!/usr/bin/env bash

die() {
    echo "$@" >&2
    exit 1
}

cd "$( dirname "$( dirname "$( readlink -f "$0" )" )" )" || die "Can't go to top dir of project?!"

cat > .git/hooks/pre-commit << _END_OF_HOOK_
#!/usr/bin/env bash
./scripts/build.full.sql.sh
git add full.create.sql
exit 0
_END_OF_HOOK_

chmod 755 .git/hooks/pre-commit

