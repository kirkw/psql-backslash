CREATE OR REPLACE FUNCTION @extschema@.center_text( p_text TEXT, p_length INT4 ) returns TEXT as $$
DECLARE
    v_prefix_len INT4;
    v_suffix_len INT4;
BEGIN
    IF length(p_text) > p_length THEN
        RETURN p_text;
    END IF;
    v_prefix_len := ( p_length - length(p_text) ) / 2;
    v_suffix_len := p_length - length(p_text) - v_prefix_len;
    RETURN format('%s%s%s', repeat(' ', v_prefix_len), p_text, repeat(' ', v_suffix_len));
END;
$$ language plpgsql
;
