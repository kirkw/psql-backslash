CREATE OR REPLACE FUNCTION @extschema@.get_text_table( p_data jsonb ) RETURNS TEXT as $$
DECLARE
    v_col_lengths INT4[];
    v_lines       TEXT[] := '{}';
BEGIN
    -- Get max length of values in each column
    WITH column_lengths AS (
        SELECT
            x.ordinality AS column_no,
            max( length( x.value ) )
        FROM
            jsonb_array_elements( p_data ) AS rows,
            jsonb_array_elements_text( rows ) WITH ORDINALITY AS x
        GROUP BY
            x.ordinality
    )
    SELECT
        array_agg( max ORDER BY column_no ) INTO v_col_lengths
    FROM
        column_lengths;

    -- Get header line and separator, formatted, into v_lines array.
    WITH header_row AS (
        SELECT
            @extschema@.center_text( x.value, v_col_lengths[x.ordinality] ) as val,
            x.ordinality
        FROM
            jsonb_array_elements_text( p_data -> 0 ) WITH ORDINALITY AS x
    )
    SELECT
        array[
            format(
                ' %s ',
                string_agg(
                    val,
                    ' | '
                    ORDER BY ordinality
                )
            ),
            format(
                '-%s-',
                string_agg(
                    repeat('-', length(val)),
                    '-+-'
                    ORDER BY ordinality
                )
            )
        ] INTO v_lines
    FROM
        header_row;

    -- Add data rows to v_lines, where all columns are left-justified.
    WITH all_rows AS (
        SELECT
            rows.ordinality AS row_no,
            ' ' || string_agg(
                rpad(
                    cols.value,
                    v_col_lengths[cols.ordinality]
                ),
                ' | '
                ORDER BY cols.ordinality
            ) || ' ' AS row_data
        FROM
            jsonb_array_elements( p_data ) WITH ORDINALITY AS rows,
            jsonb_array_elements_text( rows.value ) WITH ORDINALITY AS cols
        WHERE
            rows.ordinality > 1
        GROUP BY
            rows.ordinality
    )
    SELECT
        v_lines || array_agg( row_data ORDER BY row_no ) INTO v_lines
    FROM
        all_rows;

    RETURN array_to_string( v_lines, E'\n' );
END;
$$ language plpgsql
;
