CREATE OR REPLACE FUNCTION @extschema@.d( ) RETURNS TEXT as $$
DECLARE
    v_table_data JSONB;
    v_output TEXT;
    v_rows INT4;
    v_rows_info TEXT;
BEGIN
    SELECT
        '[ [ "Schema", "Name", "Type", "Owner" ] ]'::jsonb ||
        jsonb_agg(
            jsonb_build_array(
                n.nspname,
                c.relname,
                CASE c.relkind
                WHEN 'r' THEN
                    'table'
                WHEN 'v' THEN
                    'view'
                WHEN 'm' THEN
                    'materialized view'
                WHEN 'i' THEN
                    'index'
                WHEN 'S' THEN
                    'sequence'
                WHEN 't' THEN
                    'TOAST table'
                WHEN 'f' THEN
                    'foreign table'
                WHEN 'p' THEN
                    'partitioned table'
                WHEN 'I' THEN
                    'partitioned index'
                END,
                pg_catalog.pg_get_userbyid(c.relowner)
            )
            ORDER BY n.nspname, c.relname
        )
    INTO v_table_data
    FROM
        pg_catalog.pg_class c
        LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE
        c.relkind IN ('r', 'p', 'v', 'm', 'S', 'f', '')
        AND n.nspname <> 'pg_catalog'
        AND n.nspname !~ '^pg_toast'
        AND n.nspname <> 'information_schema'
        AND pg_catalog.pg_table_is_visible(c.oid);

    v_output := @extschema@.get_text_table( v_table_data );
    v_rows := jsonb_array_length( v_table_data ) - 1;
    IF v_rows = 1 THEN
        v_rows_info := '(1 row)';
    ELSE
        v_rows_info := format('(%s rows)', v_rows);
    END IF;

    RETURN @extschema@.center_text('List of relations', length( split_part(v_output, E'\n', 2) ) ) || E'\n' || v_output || E'\n' || v_rows_info;
END;
$$ language plpgsql
;
